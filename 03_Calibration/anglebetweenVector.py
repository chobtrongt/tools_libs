import math as math
import numpy as np



def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

""" Measurement vectors"""
FZ_slave = np.array([[193.88,2.49],[0,0.5]])
FZ_Master = np.array([[193.61,1.63],[0.83,0]])
Ref_vec = np.array([[0,0],[200,0]])

""" Determine the heading angle [deg]"""
Angle_FZ1 = angle_between((199.88,1.99,0),(-200,0,0)) * 180 /math.pi
print("Heading Slave [deg] = " + str(Angle_FZ1))

Angle_FZ2 = angle_between((192.78,1.63,0),(-200,0,0)) * 180 /math.pi
print("Heading Master [deg] = " + str(Angle_FZ2))
