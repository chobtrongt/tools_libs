import canopen
import time

network = canopen.Network()

network.connect(bustype='pcan', channel='PCAN_USBBUS1', bitrate=500000)
print("Searching ...")
network.scanner.search()
# We may need to wait a short while here to allow all nodes to respond
time.sleep(0.05)
for node_id in network.scanner.nodes:
    print("Found node %d!" % node_id)


try:
    node = network.add_node(12,'.\EAMxxx_0x0070.eds')
#network.send_message(0x0, [0x1, 0])
#node.nmt.wait_for_heartbeat()
#assert node.nmt.state == 'OPERATIONAL'

    node.nmt.state = 'OPERATIONAL'

    network.sync.start(0.1)

    device_name = node.sdo[0x1008].raw
    print(device_name)

    node.nmt.state = 'PRE-OPERATIONAL'

#0x2F,0x00,0x21,0x00,0x06
    #baudrate = node.sdo.download(0x2100,0,b'\x06')

    #print(baudrate)

     #[0x43,0x03,0x60,0x00,0x00,0x00,0x00,0x00]
    preset = node.sdo.download(0x6003,0,b'\x00\x00\x00\x00')
    print(preset)


#[0x23,0x10,0x10,0x01,0x73,0x61,0x76,0x65]
    save = baudrate = node.sdo.download(0x1010,1,b'\x73\x61\x76\x65')
    #save = baudrate = node.sdo.download(0x1010,1,b'\x65\x76\x61\x73')
    print(save)

except Exception as e:
    print(e)
finally:
    # Disconnect from CAN bus

        network.sync.stop()
        network.disconnect()
