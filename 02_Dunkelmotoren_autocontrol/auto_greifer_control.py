#!/usr/bin/env python

from __future__ import print_function

import can
from can.bus import BusState
import time


def receive_all():

    bus = can.interface.Bus()
    #bus = can.interface.Bus(bustype='ixxat', channel=0, bitrate=250000)
    #bus = can.interface.Bus(bustype='vector', app_name='CANalyzer', channel=0, bitrate=250000)

    bus.state = BusState.ACTIVE
    #bus.state = BusState.PASSIVE

    #61f Band

    msg_readbrakestatus = can.Message(arbitration_id=0x61f,
    data = [0x40,0xA0,0x49,0x01,0x05,0x00,0x00,0x00])

    msg_writebrakerelease = can.Message(arbitration_id=0x61f,
    data = [0x2F,0xA0,0x49,0x01,0x05,0x00,0x00,0x00])

    msg_write_Quick_run = can.Message(arbitration_id=0x61f,
    data = [0x2F,0x00,0x40,0x02,0x74,0x00,0x00,0x00]) #74 index 2 for VW

    msg_write_Quick_sollwert_right = can.Message(arbitration_id=0x61f,
    data = [0x23,0x00,0x40,0x11,0x00,0x01, 0x00,0x00]) # index 11

    msg_write_Quick_sollwert_left = can.Message(arbitration_id=0x61f,
    data = [0x23,0x00,0x40,0x11,0x00,0xFF,0xFF,0xFF])

    msg_read_status = can.Message(arbitration_id=0x61f,
    data = [0x40,0x02,0x40,0x01,0x00,0x00,0x00,0x00])

    msg_write_Quick_sollwert_stop = can.Message(arbitration_id=0x61f,
    data = [0x23,0x00,0x40,0x11,0x00,0x00, 0x00,0x00]) # index 11

    msg_write_Quick_stop = can.Message(arbitration_id=0x61f,
    data = [0x2F,0x00,0x40,0x02,0x71,0x00,0x00,0x00])

    #61e ARM

    msg_readbrakestatus_arm = can.Message(arbitration_id=0x61e,
    data = [0x40,0xA0,0x49,0x01,0x05,0x00,0x00,0x00])

    msg_writebrakerelease_arm = can.Message(arbitration_id=0x61e,
    data = [0x2F,0xA0,0x49,0x01,0x05,0x00,0x00,0x00])

    msg_write_Quick_run_arm = can.Message(arbitration_id=0x61e,
    data = [0x2F,0x00,0x40,0x02,0x74,0x00,0x00,0x00]) #74 index 2 for VW

    msg_write_Quick_sollwert_right_arm = can.Message(arbitration_id=0x61e,
    data = [0x23,0x00,0x40,0x11,0x00,0x01, 0x00,0x00]) # index 11

    msg_write_Quick_sollwert_left_arm = can.Message(arbitration_id=0x61e,
    data = [0x23,0x00,0x40,0x11,0x00,0xFF,0xFF,0xFF])

    msg_read_status_arm = can.Message(arbitration_id=0x61e,
    data = [0x40,0x02,0x40,0x01,0x00,0x00,0x00,0x00])

    msg_write_Quick_sollwert_stop_arm = can.Message(arbitration_id=0x61e,
    data = [0x23,0x00,0x40,0x11,0x00,0x00, 0x00,0x00]) # index 11

    msg_write_Quick_stop_arm = can.Message(arbitration_id=0x61e,
    data = [0x2F,0x00,0x40,0x02,0x71,0x00,0x00,0x00])


    print('======= Releasing brakes =======')

    #1) Release brake

    bus.send(msg_writebrakerelease)
    print('Sent release brake command to Node ID30. Recieved message:')
    time.sleep(0.05)
    msg = bus.recv(1)
    if msg is not None:
        print(msg)

    bus.send(msg_writebrakerelease_arm)
    print('Sent release brake command to Node ID31. Recieved message:')
    time.sleep(0.05)
    msg = bus.recv(1)
    if msg is not None:
        print(msg)

    time.sleep(0.05)

    # 2) Read brake status to check it again
    bus.send(msg_readbrakestatus)
    print('Asked brake status of Node ID30. Recieved message:')
    time.sleep(0.05)
    msg = bus.recv(1)
    if msg is not None:
        print(msg)

    time.sleep(0.05)
    bus.send(msg_readbrakestatus_arm)
    print('Asked brake status of Node ID31. Recieved message:')
    time.sleep(0.05)
    msg = bus.recv(1)
    if msg is not None:
        print(msg)

    mode = input('Please select mode [{r}ight / {l}eft / Only converyor {ri}ght] / Only converyor {le}ft / {Strg + C} to stop :')
    try:
        while True:

            if mode == 'r':

                print('======= Right Mode =======')
                bus.send(msg_write_Quick_run)
                print('Sent start run command to Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                time.sleep(0.05)

                bus.send(msg_write_Quick_sollwert_right)
                print('Sent sollwert speed to Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                time.sleep(0.05)

                bus.send(msg_write_Quick_run_arm)
                print('Sent start run command to Node ID31. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                time.sleep(0.05)

                bus.send(msg_write_Quick_sollwert_right_arm)
                print('Sent sollwert speed to Node ID31. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                time.sleep(0.05)

            elif mode == 'l':
                print('======= Left Mode, Only band =======')
                bus.send(msg_write_Quick_run)
                print('Sent start run command to Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                time.sleep(0.05)

                bus.send(msg_write_Quick_sollwert_left)
                print('Sent sollwert speed to Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                time.sleep(0.05)

                bus.send(msg_write_Quick_run_arm)
                print('Sent start run command to Node ID31. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                time.sleep(0.05)

                bus.send(msg_write_Quick_sollwert_left_arm)
                print('Sent sollwert speed to Node ID31. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                time.sleep(0.05)

            elif mode == 'ri':

                print('======= Right Mode , Only band =======')
                bus.send(msg_write_Quick_run)
                print('Sent start run command to Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                time.sleep(0.05)

                bus.send(msg_write_Quick_sollwert_right)
                print('Sent sollwert speed to Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                time.sleep(0.05)

            elif mode == 'le':
                print('======= Left Mode =======')
                bus.send(msg_write_Quick_run)
                print('Sent start run command to Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                time.sleep(0.05)

                bus.send(msg_write_Quick_sollwert_left)
                print('Sent sollwert speed to Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                time.sleep(0.05)

            elif mode == 'p':
                print('======= Pausing =======')
                bus.send(msg_write_Quick_stop)
                print('Sent stop command to Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                    time.sleep(0.05)

                bus.send(msg_write_Quick_sollwert_stop)
                print('Set sollwert speed to 0 fo Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)

                bus.send(msg_write_Quick_stop_arm)
                print('Sent stop command to Node ID31. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                    time.sleep(0.05)

                bus.send(msg_write_Quick_sollwert_stop_arm)
                print('Set sollwert speed to 0 fo Node ID31. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)

                mode = input('Please select mode [{q}uit / {r}ight / {l}eft / Only converyor {le}ft / Only converyor {ri}ght] / {Strg + C} to stop :')

            elif mode == 'q':
                print('======= Quiting =======')
                bus.send(msg_write_Quick_stop)
                print('Sent stop command to Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                    time.sleep(0.05)

                bus.send(msg_write_Quick_sollwert_stop)
                print('Set sollwert speed to 0 fo Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)

                bus.send(msg_write_Quick_stop_arm)
                print('Sent stop command to Node ID31. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                    time.sleep(0.05)

                bus.send(msg_write_Quick_sollwert_stop_arm)
                print('Set sollwert speed to 0 fo Node ID31. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)

                print('======= END =======')

            else:
                print('======= Unknown Mode, Terminating the routine =======')
                bus.send(msg_write_Quick_stop)
                print('Sent stop command to Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                    time.sleep(0.05)

                bus.send(msg_write_Quick_sollwert_stop)
                print('Set sollwert speed to 0 fo Node ID30. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)

                bus.send(msg_write_Quick_stop_arm)
                print('Sent stop command to Node ID31. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)
                    time.sleep(0.05)

                bus.send(msg_write_Quick_sollwert_stop_arm)
                print('Set sollwert speed to 0 fo Node ID31. Recieved message:')
                msg = bus.recv(1)
                if msg is not None:
                    print(msg)

    except KeyboardInterrupt:
            print('======= Terminating =======')
            bus.send(msg_write_Quick_stop)
            print('Sent stop command to Node ID30. Recieved message:')
            msg = bus.recv(1)
            if msg is not None:
                print(msg)
                time.sleep(0.05)

            bus.send(msg_write_Quick_sollwert_stop)
            print('Set sollwert speed to 0 fo Node ID30. Recieved message:')
            msg = bus.recv(1)
            if msg is not None:
                print(msg)

            bus.send(msg_write_Quick_stop_arm)
            print('Sent stop command to Node ID31. Recieved message:')
            msg = bus.recv(1)
            if msg is not None:
                print(msg)
                time.sleep(0.05)

            bus.send(msg_write_Quick_sollwert_stop_arm)
            print('Set sollwert speed to 0 fo Node ID31. Recieved message:')
            msg = bus.recv(1)
            if msg is not None:
                print(msg)

            isquit = input('Would you like to quit? {y}es/{n}o :')

            if isquit == 'y':
                print('======= Done =======')
                exit()
            else :
                pass


if __name__ == "__main__":
    receive_all()
