#-----------------------------------------------------------------------------------------------------------------------
# Parameter: Knotenadresse, CobIds, Uebertragungsart
NodeId = 30                                                       # Knotenadresse ARM: 30, Kette 31
TXCobId = 0x180 + NodeId                                          # CobId fuer das erste TX-PDO (Motor sendet)
RXCobId = 0x200 + NodeId                                          # CobId fuer das erste RX-PDO (Motor empfaegt)
Transmissiontype = 255                                            # Uebertragungsart asynchron
#Transmissiontypes                                                # Wert (dec) Typ
                                                                  # 0            acyclic synchronous
                                                                  # 1 ... 240    cyclic synchronou
                                                                  # 241 ... 251  reserviert
                                                                  # 252          synchronous nur RTR
                                                                  # 253          asynchronous nur RTR
                                                                  # 254 ... 255  asynchronous

#-----------------------------------------------------------------------------------------------------------------------
# Erstes RX-PDO (Motor empfaegt)

# CobId loeschen
SdoWr(NodeId, 0x1400, 1, 0xC0000000, DataType.UInt32, 1000)       # Zuerst Loeschen der alten CobId (Bit 30+31 = 1)

# Mapping Tabelle
SdoWr(NodeId, 0x1600, 0, 0, DataType.UInt8)                       # Zuerst Mapping Tabelle loeschen
SdoWr(NodeId, 0x1600, 1, 0x40001220, DataType.UInt32)             # erstes Objekt eintragen (Quickstart - Sollwert 1):
                                                                  # Objekt 4000.12h (0x400012), Laenge = 32 bits (0x20h=>0x40001220)
SdoWr(NodeId, 0x1600, 2, 0x40001310, DataType.UInt16)             # zweites Objekt eintragen (Quickstart - Sollwert 0 - Int16):
                                                                  # Objekt 4000.13h (0x400013), Laenge = 16 bits (0x10=>0x40001310)
SdoWr(NodeId, 0x1600, 3, 0x40000208, DataType.UInt8)             # drittes Objekt eintragen (Quickstart - Geraetekommando - Int16):
                                                                  # Objekt 4000.02h (0x400002), Laenge = 8 bits (0x8h=>0x40000208)
SdoWr(NodeId, 0x1600, 4, 0x49A00108, DataType.UInt8)             # 4. Objekt eintragen (Bremsenmanagement):
                                                                  # Objekt 49A0.01h (0x49A001), Laenge = 8 bits (0x08h=>0x49A00108)

SdoWr(NodeId, 0x1600, 0, 4, DataType.UInt8)                       # Anzahl der Objekte in die Tabelle eintragen = 3

# Uebertragungsart
SdoWr(NodeId, 0x1400, 2, 255, DataType.UInt8)                     # Ereignisgesteuert (s.o.)

# CobId setzen
SdoWr(NodeId, 0x1400, 1, RXCobId, DataType.Int32)                 # Neue CobId setzen

#-----------------------------------------------------------------------------------------------------------------------
# Erstes TX-PDO (Motor sendet)

# CobId loeschen
SdoWr(NodeId, 0x1800, 1, 0x80000000, DataType.UInt32, 1000)       # Zuerst Loeschen der alten CobId (Bit 31=1)

# Mapping Tabelle
SdoWr(NodeId, 0x1A00, 0, 0, DataType.UInt8)                       # Zuerst Mapping Tabelle loeschen
SdoWr(NodeId, 0x1A00, 1, 0x47620120, DataType.UInt32)             # erstes Objekt eintragen (aktuelle Position):
                                                                  # Objekt 4762.01h (0x476201), Laenge = 32 bits (0x20h=>0x47620120)
SdoWr(NodeId, 0x1A00, 2, 0x40020110, DataType.UInt32)             # zweites Objekt eintragen (Statusregister):
                                                                  # Objekt 4002.01h (0x400201), Laenge = 16 bits (0x10h=>0x40020110)
SdoWr(NodeId, 0x1A00, 3, 0x41200108, DataType.UInt32)             # drittes Objekt eintragen (digitale Eingaenge):
                                                                  # Objekt 4120.01h (0x412001), Laenge = 8 bits (0x08h=>0x41200108)
SdoWr(NodeId, 0x1A00, 4, 0x49A00108, DataType.UInt8)             # viertes Objekt eintragen (Bremsenmanagement):
                                                                  # Objekt 49A0.01h (0x49A001), Laenge = 8 bits (0x08h=>0x49A00108)
SdoWr(NodeId, 0x1A00, 0, 4, DataType.UInt8)                       # Anzahl der Objekte in die Tabelle eintragen = 4

# Uebertragungsart
SdoWr(NodeId, 0x1800, 2, 255, DataType.UInt8)                     # Ereignisgesteuert (s.o.)
SdoWr(NodeId, 0x1800, 3, 200, DataType.UInt16)                    # Unterdruekungszeit (inhibit time) in 100ys

# CobId setzen
SdoWr(NodeId, 0x1800, 1, TXCobId, DataType.UInt32)                # Neue CobId setzen

#-----------------------------------------------------------------------------------------------------------------------
PdoWr(0x0, [0x01, 0])                                             # NMT Start

# Speichern im EEPROM
SdoWr(NodeId, 0x1010, 2, 0x65766173, DataType.Int32, 10000)      # Kommunikationsparameter im EEPROM speichern

print "PDO-Mapping wurde erstellt!"
