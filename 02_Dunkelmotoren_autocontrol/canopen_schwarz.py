import canopen
import time

network = canopen.Network()

network.connect(bustype='pcan', channel='PCAN_USBBUS1', bitrate=500000)
print("Searching ...")
network.scanner.search()
# We may need to wait a short while here to allow all nodes to respond
time.sleep(0.05)
for node_id in network.scanner.nodes:
    print("Found node %d!" % node_id)


try:
    node = network.add_node(11,'.\schwarz_invertor.eds')
#network.send_message(0x0, [0x1, 0])
#node.nmt.wait_for_heartbeat()
#assert node.nmt.state == 'OPERATIONAL'

    node.nmt.state = 'OPERATIONAL'

    network.sync.start(0.1)

    # Read current PDO configuration
    node.tpdo.read()

    print(node.tpdo)



except Exception as e:
    print(e)
finally:
    # Disconnect from CAN bus

        network.sync.stop()
        network.disconnect()
