from __future__ import print_function

import can
from can.bus import BusState
import time

def send_one_and_listen():
    bus = can.interface.Bus()

    bus.state = BusState.ACTIVE

    Sync = can.Message(arbitration_id=0x080)

    ''' id 12 '''

    NMT_Preoperation_ID12 = can.Message(arbitration_id=0x60C,
    data = [0x4F,0x80,0x00,0x00,0x00,0x00,0x00,0x00])

    NMT_Start_ID12 = can.Message(arbitration_id=0x60C,
    data = [0x4F,0x01,0x00,0x00,0x00,0x00,0x00,0x00])



    msg_set_baudrate_ID12 = can.Message(arbitration_id=0x60C,
    data = [0x43,0x21,0x33,0x00,0x05,0x00,0x00,0x00])

    msg_set_PDO1_Dis_ID12 = can.Message(arbitration_id=0x60C,
    data = [0x43,0x00,0x18,0x02,0xFD,0x00,0x00,0x00])

    msg_set_PDO2_Sync_ID12 = can.Message(arbitration_id=0x60C,
    data = [0x43,0x01,0x18,0x02,0x01,0x00,0x00,0x00])

    msg_set_preset_ID12 = can.Message(arbitration_id=0x60C,
    data = [0x43,0x03,0x60,0x00,0x00,0x00,0x00,0x00])

    msg_saveEEPROM_ID12 = can.Message(arbitration_id=0x60C,
    data = [0x43,0x10,0x10,0x01,0x73,0x61,0x76,0x65])


    ''' id 22 '''

    NMT_Preoperation_ID13 = can.Message(arbitration_id=0x616,
    data = [0x4F,0x80,0x00,0x00,0x00,0x00,0x00,0x00])

    #NMT_Start_ID13 = can.Message(arbitration_id=0x00,
    #data = [0x4B,0x01,0x00,0x00,0x00,0x00,0x00,0x00])

    NMT_Start_ID13 = can.Message(arbitration_id=0x00,
    data = [0x01,0x00])



    msg_set_baudrate_ID13 = can.Message(arbitration_id=0x616,
    data = [0x43,0x21,0x33,0x00,0x06,0x00,0x00,0x00])

    msg_set_PDO1_Dis_ID13 = can.Message(arbitration_id=0x616,
    data = [0x43,0x00,0x18,0x02,0xFD,0x00,0x00,0x00])

    msg_set_PDO2_Sync_ID13 = can.Message(arbitration_id=0x616,
    data = [0x43,0x01,0x18,0x02,0x01,0x00,0x00,0x00])

    msg_set_preset_ID13 = can.Message(arbitration_id=0x616,
    data = [0x43,0x03,0x60,0x00,0x00,0x00,0x00,0x00])

    msg_saveEEPROM_ID13 = can.Message(arbitration_id=0x616,
    data = [0x43,0x10,0x10,0x01,0x73,0x61,0x76,0x65])


    try:
        bus.send(NMT_Start_ID13)
        print(NMT_Start_ID13)
    except can.CanError:
        print("Cannot send message")
    time.sleep(0.50)
    msg = bus.recv(1)
    if msg is not None:
        print(msg)

    time.sleep(0.05)

    while True:
        try:
            bus.send(Sync)
            print(Sync)
        except can.CanError:
            print("Cannot send message")

        time.sleep(1)
        try:
            msg = bus.recv(1)
            if msg is not None:
                print(msg)

                time.sleep(0.05)
        except can.CanError:
            print("Cannot get message")


if __name__ == '__main__':
    send_one_and_listen()
