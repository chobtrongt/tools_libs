from __future__ import print_function

import can
from can.bus import BusState
import time

def send_one_and_listen():
    bus = can.interface.Bus()

    bus.state = BusState.ACTIVE

    Sync = can.Message(arbitration_id=0x80)

    ''' id 12 '''

    NMT_Request = can.Message(arbitration_id=0x60C,
    data = [0x40,0x00,0x10,0x00,0x00,0x00,0x00,0x00])

    NMT_Preoperation_ID12 = can.Message(arbitration_id=0x000,
    data = [0x80,0x0C])

    NMT_Start_ID12 = can.Message(arbitration_id=0x000,
    data = [0x01,0x0C])



    msg_set_baudrate_ID12 = can.Message(arbitration_id=0x60C,
    data = [0x2F,0x00,0x21,0x00,0x06])

    msg_set_PDO1_Dis_ID12 = can.Message(arbitration_id=0x60C,
    data = [0x43,0x00,0x18,0x02,0xFD,0x00,0x00,0x00])

    msg_set_PDO2_Sync_ID12 = can.Message(arbitration_id=0x60C,
    data = [0x43,0x01,0x18,0x02,0x01,0x00,0x00,0x00])

    msg_set_preset_ID12 = can.Message(arbitration_id=0x60C,
    data = [0x43,0x03,0x60,0x00,0x00,0x00,0x00,0x00])

    msg_saveEEPROM_ID12 = can.Message(arbitration_id=0x60C,
    data = [0x23,0x10,0x10,0x01,0x73,0x61,0x76,0x65])


    ''' id 22 '''

    #NMT_Preoperation_ID13 = can.Message(arbitration_id=0x00,
    #data = [0x4B,0x80,0x00,0x00,0x00])

    SDO_Request_ID22 = can.Message(arbitration_id=0x616,
    data = [0x40,0x00,0x10,0x00,0x00,0x00,0x00,0x00])

    NMT_Preoperation_ID22 = can.Message(arbitration_id=0x00,
    data = [0x80,0x00])

    NMT_Reset_ID22 = can.Message(arbitration_id=0x00,
    data = [0x81,0x00])

    NMT_Start_ID22 = can.Message(arbitration_id=0x00,
    data = [0x01,0x00])

    #Test with 580 + Node ID (22-> 596, 12-> 58C )

    msg_set_baudrate_ID22 = can.Message(arbitration_id=0x616,
    data = [0x4F,0x00,0x21,0x00,0x06])

    msg_set_PDO1_Dis_ID22 = can.Message(arbitration_id=0x616,
    data = [0x4F,0x00,0x18,0x02,0xFD])

    msg_set_PDO2_Sync_ID22 = can.Message(arbitration_id=0x616,
    data = [0x4F,0x01,0x18,0x02,0x01])

    msg_set_preset_ID22 = can.Message(arbitration_id=0x616,
    data = [0x43,0x03,0x60,0x00,0x00,0x00,0x00,0x00])

    msg_saveEEPROM_ID22 = can.Message(arbitration_id=0x616,
    data = [0x43,0x10,0x10,0x01,0x65,0x76,0x61,0x73])

    msg_ResetComm_param_ID22 = can.Message(arbitration_id=0x616,
    data = [0x43,0x11,0x10,0x02,0x64,0x61,0x6F,0x6C])


    try:
        bus.send(msg_set_baudrate_ID12)
        print(msg_set_baudrate_ID12)
    except can.CanError:
        print("Cannot send message")

    time.sleep(1)
    isOK = False
    while (isOK == False):
        time.sleep(1)
        msg = bus.recv(1)
        if msg is not None:
            print(msg)
            isOK = True

if __name__ == '__main__':
    send_one_and_listen()
