from bokeh.plotting import figure,show

import csv

tagList_FWD = list()
pointXList_FWD = list()
pointYList_FWD = list()
movementList_FWD = list()

tagList_BWD = list()
pointXList_BWD = list()
pointYList_BWD = list()
movementList_BWD = list()

tagList_BOX = list()
pointXList_BOX = list()
pointYList_BOX = list()
movementList_BOX = list()

tagList_BOXOUT = list()
pointXList_BOXOUT = list()
pointYList_BOXOUT = list()
movementList_BOXOUT = list()


with open('C:\\VM_Beckhoff_ShareFolder\\workspace\\tuenkers\\tuenkers-laepple\\TrafficWarden\\UnitTest.Core\\bin\Debug\\test_route.csv',newline = '\n') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    next(reader,None)

    for row in reader:
        movetype = row[3]
        tempX = float(row[1].replace(',','.'))
        tempY = float(row[2].replace(',','.'))

        if movetype == "MOVE_AHEAD" :
            tagList_FWD.append(row[0])
            pointXList_FWD.append(tempX)
            pointYList_FWD.append(tempY)
            movementList_FWD.append(movetype)

        elif movetype == "MOVE_BACKWARDS" :
            tagList_BWD.append(row[0])
            pointXList_BWD.append(tempX)
            pointYList_BWD.append(tempY)
            movementList_BWD.append(movetype)

        elif movetype == "MOVE_ENTERING_BOX" :
            tagList_BOX.append(row[0])
            pointXList_BOX.append(tempX)
            pointYList_BOX.append(tempY)
            movementList_BOX.append(movetype)

        elif movetype == "MOVE_OUT_BOX" :
            tagList_BOXOUT.append(row[0])
            pointXList_BOXOUT.append(tempX)
            pointYList_BOXOUT.append(tempY)
            movementList_BOXOUT.append(movetype)


#p = figure(x_axis_label='x', y_axis_label='y', x_range=[0,3000], y_range=[0, 3000])
p = figure(x_axis_label='x', y_axis_label='y')
p.line(pointXList_FWD,pointYList_FWD, line_width=2,line_color='blue')
p.line(pointXList_BWD,pointYList_BWD, line_width=2,line_color='red')
p.line(pointXList_BOX,pointYList_BOX, line_width=2,line_color='black')
p.line(pointXList_BOXOUT,pointYList_BOXOUT, line_width=2,line_color='green')
show(p)
