from bokeh.plotting import figure,show
from bokeh.palettes import brewer

import csv
import os
import tkinter as tk
from tkinter import filedialog

print(">>> START")

root = tk.Tk()
root.withdraw()
rawfilename = filedialog.askopenfilename()

pointXList = list()
pointYList = list()
HeadingList = list()
dataref = list()
refpointX = list([1000, 1500, 1500, 1000,1000])
refpointY = list([900,900,1200,1200,900])

#rawfilename = "C:\\VM_Beckhoff_ShareFolder\\workspace\\Projects\\01_Laepple\\01_PO992\\Messdaten\\02_CrossDrive_20180801\\crossdrive_20180801.txt"
targetdata = "temp.txt"
savefile = rawfilename.replace('.txt','.csv')

# Read raw data
print(">>> Reading raw data ...")
with open(rawfilename) as f:
    content = f.readlines()

content = [x.strip() for x in content]

# Sort refpoint data
print(">>> Sorting data ...")
for data in content :
    if ">>> DEBUG: RefP:" in data :
            dataref.append(data)

# write to target
with open(targetdata,'w') as ftarget:
    for data in dataref :
        ftarget.write(str(data) + '\n')

# Read csv
print(">>> Ploting data ...")
with open(targetdata, newline = '\n') as csvfile:
    reader = csv.reader(csvfile, delimiter=':')
    for row in reader :
        tempX = float(row[5].replace(',','.'))
        tempY = float(row[6].replace(',','.'))
        tempH = float(row[7].replace(',','.'))

        #print(tempX);

        pointXList.append(tempX)
        pointYList.append(tempY)
        HeadingList.append(tempH)


colors = brewer['Spectral'][4]

p = figure(title="AGV position",x_axis_label='x [cm]', y_axis_label='y [cm]', x_range=[800,1600], y_range=[800, 1600])
# p = figure(x_axis_label='x axis', y_axis_label='y axis')
p.line(pointXList,pointYList, line_width=1,line_color=colors[0], legend="AGV Pos")
p.circle(pointXList,pointYList,color=colors[0],fill_alpha=0.4,size=2, legend="AGV Pos")
# p.line(refpointX,refpointY, line_width=2,line_color=colors[3])
p.scatter(refpointX,refpointY, line_width=2,fill_color=colors[3],fill_alpha=0.6, line_color = None, radius = 5, legend="Tag Pos")
p.scatter([pointXList[0]],[pointYList[0]], line_width=2,line_color=colors[1],fill_alpha=0.6,marker='cross',size=15)
#show(p)

Time = list()

for index in range(0,len(pointXList)):
    Time.append(index)

p2 = figure(title="Test")
p2.line(Time,pointXList,line_color=colors[0])
p2.line(Time,pointYList,line_color=colors[1])
show(p2)
show(p)

# Export csv data [DE format]
print(">>> Exporting data ...")
with open(savefile,'w', newline='') as csvfile:
    fieldnames = ['X [cm]', 'Y [cm]','Heading [deg]']
    writer = csv.DictWriter(csvfile, delimiter =';',fieldnames=fieldnames)
    writer.writeheader()
    i = 0
    while (i < len(pointXList)) :
        writer.writerow({'X [cm]' : str(pointXList[i]).replace('.',','), 'Y [cm]' : str(pointYList[i]).replace('.',','),'Heading [deg]':str(HeadingList[i]).replace('.',',')})
        i += 1

# Delete temp File
os.remove(targetdata)
print(">>> FINISH")
